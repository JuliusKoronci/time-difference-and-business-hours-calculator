<?php
namespace Websolutions\WorkingHours;

require_once('iWorkingHours.php');
require_once('WorkingHours.php');

$workingHours = new WorkingHours();

$workingHours->setFrom(new \DateTime('22.11.2014 6:54'));
$workingHours->setTo(new \DateTime());
$workingHours->setBusinessHours(true);
$workingHours->setUseStandardHours(false);

//$time1 = $workingHours->getUnixTimeFormat('12:00');
//$time2 = $workingHours->getUnixTimeFormat('09:30');
//
//
//$date = new \DateTime();
//$date->setTimestamp($time1);
//$date2 = new \DateTime();
//$date2->setTimestamp($time2);
//
//$interval = $date->diff($date2);
//echo (int)$interval->format('%r%h');

echo $workingHours->calculateDifferenceInHours();