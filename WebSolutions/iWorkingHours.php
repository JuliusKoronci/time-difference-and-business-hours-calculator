<?php
/**
 * Created by PhpStorm.
 * User: web-solutions
 * Date: 24.11.2014
 * Time: 8:22
 */

namespace Websolutions\WorkingHours;


interface iWorkingHours
{

    public function getWorkingHours($from, $to);

    public function getWorkingHoursDatetime(\DateTime $from, \DateTime $to);

    public function getWorkingHoursFromNow($from);

}