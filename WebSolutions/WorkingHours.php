<?php
/**
 * Created by PhpStorm.
 * User: web-solutions
 * Date: 24.11.2014
 * Time: 8:22
 */

namespace Websolutions\WorkingHours;

/**
 * Calculates working hours between dates
 *
 * Considers business hours, weekends, custom dates
 *
 * Class WorkingHours
 * @namespace Websolutions
 */
class WorkingHours implements iWorkingHours
{

    /**
     * @var string $timeZone = Europe/Bratislava
     */
    private $timeZone = 'Europe/Bratislava';

    /**
     * First key is referencing datetime value for day, holds 2 values: from, to
     *
     * @var array week days e.g. ----  1 => array('08:00', '17:00', false)
     */
    private $days = array(
        0 => array('08:00', '17:00'),
        1 => array('08:00', '17:00'),
        2 => array('08:00', '17:00'),
        3 => array('08:00', '17:00'),
        4 => array('08:00', '17:00'),
        5 => array('08:00', '17:00'),
        6 => array('08:00', '17:00'),
    );

    /*
     * days to convert
     */
    private static $dayToInt = array(
        'Sunday' => 0,
        'Monday' => 1,
        'Tuesday' => 2,
        'Wednesday' => 3,
        'Thursday' => 4,
        'Friday' => 5,
        'Saturday' => 6,
    );
    /** @var array from, to used if no custom dates specified */
    private $standardWorkingHours = array('08:00', '17:00');

    /**
     * If true business hours will be considered else we use 24/7 or 24/5
     * @var bool
     */
    private $businessHours = false;

    /** @var bool Use default business hours for all days or custom */
    private $useStandardHours = true;

    /** @var array weekend days in string format */
    private $weekend = array(0, 6);

    /** @var bool if weekends should be included in calculations */
    private $useWeekend = false;

    /** @var array Holidays e.g. array('15.01.2014',..) */
    private $holidays = array();

    /** @var  \DateTime $from */
    private $from;

    /** @var  \DateTime $to */
    private $to;

    /**
     * @param bool $businessHours
     */
    public function __construct($businessHours = false)
    {
        $this->businessHours = $businessHours;
    }

    /**
     * Returns working hours between two dates in string format
     * Considers business hours and weekends based on settings
     *
     * @param string $from
     * @param string $to
     */
    public function getWorkingHours($from, $to)
    {
        $this->from = $this->getDatetime($from);
        $this->to = $this->getDatetime($to);
    }

    /**
     * Returns working hours between a selected date in string format and current date and time
     * Considers business hours and weekends based on settings
     * @param string $from
     */
    public function getWorkingHoursFromNow($from)
    {
        $this->from = $this->getDatetime($from);
        $this->to = $this->getDatetime();

    }

    /**
     * Returns working hours between two dates in Datetime format
     * Considers business hours and weekends based on settings
     *
     * @param \DateTime $from
     * @param \DateTime $to
     */
    public function getWorkingHoursDatetime(\DateTime $from, \DateTime $to)
    {
        $this->from = $from;
        $this->to = $to;
    }

    /**
     * Helper methods
     */

    /**
     * Returns a difference between two datetime variables in hours
     *
     * Considers business hours, weekends, standard or custom working hours for days
     *
     *
     * @throws \Exception
     *
     * @return int $hours
     */
    public function calculateDifferenceInHours()
    {
        if (empty($this->from) OR empty($this->to)) {
            throw new \Exception('Missing dates: from or to or both');
        }
        /**
         * if from and to are on the same dates, we need to return only difference in hours
         */
        if ($this->from->format('d-m-y') == $this->to->format('d-m-y')) {


            if ($this->businessHours) {
                if ($this->useStandardHours) {
                    return $this->getDifferenceByStandardHours();
                }
                if (!$this->useStandardHours) {
                    return $this->getDifferenceByCustomHours($this->from->format('w'));
                }
            } else {
                return $this->from->diff($this->to)->format('%r%h');
            }

        }

        if ($this->from->format('d-m-y') > $this->to->format('d-m-y')) {
            throw new \Exception('"From" date can not be greater than "to" date');
        }

        if ($this->from->format('d-m-y') < $this->to->format('d-m-y')) {
            //we need to get the summary for each day, sum it up
            //we need to consider: weekends, standard dates and custom dates
            //we need to iterate trough each day, check weekends, and then process based on standard hours config

            $summary = 0;
            do {

                if (!$this->useWeekend) {
                    //don't use weekends if not allowed
                    if (in_array($this->from->format('w'), $this->weekend)) {
                        continue;
                    };
                }
                echo $this->from->format('d-m-y') . '<br>';
                if ($this->businessHours) {
                    if ($this->useStandardHours) {
                        $summary += $this->getDifferenceByStandardHours(true);
                    }
                    if (!$this->useStandardHours) {
                        $summary += $this->getDifferenceByCustomHours($this->from->format('w'), true);
                    }
                } else {
                    if ($this->from->format('d-m-y') == $this->to->format('d-m-y')) {
                        $summary += $this->from->diff(new \DateTime())->format('%r%h');
                    } else {
                        $summary += 24;
                    }

                }

            } while ($this->from->modify('+ 1 day')->format('d-m-y') <= $this->to->format('d-m-y'));

            return $summary;
        }
        return 'something went wrong!';
    }

    /**
     * Gets the difference in hours by standard times for Hours of the same day
     *
     * @internal param int $fromHours
     * @internal param int $toHours
     * @param bool $wholeDay
     *
     * @return number
     */
    protected function getDifferenceByStandardHours($wholeDay = false)
    {
        $standardFrom = new \DateTime();
        $standardFrom->setTimestamp($this->getUnixTimeFormat($this->standardWorkingHours[0]));

        $standardTo = new \DateTime();
        $standardTo->setTimestamp($this->getUnixTimeFormat($this->standardWorkingHours[1]));

        if ($wholeDay AND $this->from->format('d') != $this->to->format('d')) {
            return (int)$standardTo->format('G') - (int)$standardFrom->format('G');
        }

        $fromHours = $this->from->format('G');
        $toHours = $this->to->format('G');


        if ($this->from->format('G') < $standardFrom->format('G')) {
            $fromHours = $standardFrom->format('G');
        }

        if ($this->to->format('G') > $standardTo->format('G')) {

            $toHours = $standardTo->format('G');
        }

        return (int)$toHours - (int)$fromHours;
    }

    /**
     * Gets the difference in hours by custom times for Hours of the same day
     *
     * @param int $day digital representation of the day of week specified by date('w')
     *
     * @param bool $wholeDay
     * @internal param int $fromHours
     * @internal param int $toHours
     *
     * @return number
     */
    protected function getDifferenceByCustomHours($day, $wholeDay = false)
    {
        $standardFrom = new \DateTime();
        $standardFrom->setTimestamp($this->getUnixTimeFormat($this->days[$day][0]));

        $standardTo = new \DateTime();
        $standardTo->setTimestamp($this->getUnixTimeFormat($this->days[$day][1]));

        if ($wholeDay AND $this->from->format('d') != $this->to->format('d')) {
            return (int)$standardTo->format('G') - (int)$standardFrom->format('G');
        }

        $fromHours = $this->from->format('G');
        $toHours = $this->to->format('G');

        if ($this->from->format('G') < $standardFrom->format('G')) {
            $fromHours = $standardFrom->format('G');
        }

        if ($this->to->format('G') > $standardTo->format('G')) {

            $toHours = $standardTo->format('G');
        }

        return (int)$toHours - (int)$fromHours;
    }

    /**
     * Converts string to datetime, if null returns Now
     *
     * @param string|null $timeString
     *
     * @return \DateTime
     */
    protected function getDatetime($timeString = null)
    {
        if (null === $timeString) {
            return new \DateTime();
        }
        return new \DateTime($timeString);
    }

    /**
     * Converts string representation of time to unix time format
     *
     * @param string $time hour of a day e.g. 08:00
     *
     * @return int Unix time format
     */
    public function getUnixTimeFormat($time)
    {
        try {
            list($hours, $minutes) = explode(':', $time);
            return mktime($hours, $minutes);
        } catch (\Exception $e) {
            return mktime((int)$time);
        }
    }

    /**
     * getters and setters
     */


    /**
     * @param boolean $businessHours
     */
    public function setBusinessHours($businessHours)
    {
        $this->businessHours = $businessHours;
    }

    /**
     * @return boolean
     */
    public function getBusinessHours()
    {
        return $this->businessHours;
    }

    /**
     * @param array $days
     */
    public function setDays($days)
    {
        $this->days = $days;
    }

    /**
     * @return array
     */
    public function getDays()
    {
        return $this->days;
    }

    /**
     * @param \DateTime $from
     */
    public function setFrom($from)
    {
        $this->from = $from;
    }

    /**
     * @return \DateTime
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @param array $holidays
     */
    public function setHolidays($holidays)
    {
        $this->holidays = $holidays;
    }

    /**
     * @return array
     */
    public function getHolidays()
    {
        return $this->holidays;
    }

    /**
     * @param array $standardWorkingHours
     */
    public function setStandardWorkingHours($standardWorkingHours)
    {
        $this->standardWorkingHours = $standardWorkingHours;
    }

    /**
     * @return array
     */
    public function getStandardWorkingHours()
    {
        return $this->standardWorkingHours;
    }

    /**
     * @param string $timeZone
     */
    public function setTimeZone($timeZone)
    {
        $this->timeZone = $timeZone;
    }

    /**
     * @return string
     */
    public function getTimeZone()
    {
        return $this->timeZone;
    }

    /**
     * @param \DateTime $to
     */
    public function setTo($to)
    {
        $this->to = $to;
    }

    /**
     * @return \DateTime
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @param boolean $useStandardHours
     */
    public function setUseStandardHours($useStandardHours)
    {
        $this->useStandardHours = $useStandardHours;
    }

    /**
     * @return boolean
     */
    public function getUseStandardHours()
    {
        return $this->useStandardHours;
    }

    /**
     * Needs to be in int format of date('w') type with convert false or English textual representation with convert true
     *
     * @param array $weekend
     * @param bool $convert = true
     *
     * @throws \Exception
     */
    public function setWeekend(array $weekend, $convert = true)
    {
        if ($convert) {
            $weekendConverted = array();
            foreach ($weekend as $values) {
                if (!in_array($values, self::$dayToInt)) {
                    throw new \Exception('Unsupported day format, use english names e.g. array("Sunday")');
                }
                $weekendConverted[] = self::$dayToInt[$values];
            }
            $this->weekend = $weekendConverted;
        } else {
            $this->weekend = $weekend;
        }
    }

    /**
     * @return array
     */
    public function getWeekend()
    {
        return $this->weekend;
    }


}