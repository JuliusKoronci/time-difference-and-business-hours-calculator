Working Hours

Returns the difference between two dates in hours

- considers business days
- considers weekends
- custom holidays

Task yet to do:

- Complete refactoring
- Missing documentation
- Examples
- Unit tests
- Functional tests
